suphp (0.7.1-1.hcoop2) stable; urgency=low

  * Build with setid-mode=force to prevent weirdness when users upload files using afs

 -- Clinton Ebadi <clinton@unknownlamer.org>  Fri, 08 Feb 2013 13:32:56 -0500

suphp (0.7.1-1.hcoop1) stable; urgency=low

  * Don't nuke git/svn information during build. It's not polite to zap the package's own version history!
  * Build with setid-mode=paranoid The Debian default, owner, is insufficient for HCoop.

 -- Clinton Ebadi <clinton@unknownlamer.org>  Fri, 07 Sep 2012 17:28:09 -0400

suphp (0.7.1-1) unstable; urgency=low

  * New upstream release (closes: #528379, #520182) 
  * debian/NEWS: add information about AddHandler -> AddType change introduced
    in 0.6.2-2 (closes: #517805)
  * debian/conf/suphp.conf, debian/patches/01_debian.dpatch: switch from
    application/x-httpd-php to application/x-httpd-suphp to allow
    simultaneous use of mod_suphp and mod_php (closes: #519005, #514725)

 -- Emmanuel Lacour <elacour@home-dn.net>  Mon, 03 Aug 2009 15:15:38 +0200

suphp (0.6.2-3) unstable; urgency=low

  * debian/conf/suphp.conf: by default, deactivate suPHP for applications in
    /usr/share (owned by uid 0) (closes: #472352, closes: #420007)
  * debian/control: bump standard-version

 -- Emmanuel Lacour <elacour@home-dn.net>  Wed, 04 Jun 2008 10:04:08 +0200

suphp (0.6.2-2.1) unstable; urgency=high

  * Non-maintainer upload by the security team
  * Fix race condition in symlink handling by adding
    04_CVE-2008-1614.dpatch (Closes: #475431) Fixes: CVE-2008-1614

 -- Steffen Joeris <white@debian.org>  Sat, 10 May 2008 08:48:45 +0000

suphp (0.6.2-2) unstable; urgency=low

  * remove apache 1.x package (closes: #429079)
  * debian/rules, debian/compat, debian/control: lintian cleanup
  * debian/conf/suphp.conf, debian/patches/01_debian.dpatch: replaced
    AddHandler by AddType and x-httpd-php by application/x-httpd-php to get
    the same behavior as mod php with filenames extensions (closes: #416424)

 -- Emmanuel Lacour <elacour@home-dn.net>  Sun, 14 Oct 2007 19:42:30 +0200

suphp (0.6.2-1) unstable; urgency=low

  * New uptream release (closes: #405059)
    (no sources changes from previous snapshot)

 -- Emmanuel Lacour <elacour@home-dn.net>  Tue,  9 Jan 2007 18:29:26 +0100

suphp (0.6.1.20061108-1) unstable; urgency=low

  * New upstream snapshot (closes: #395929)
  * debian/patches/04_apache2.2.dpatch: removed (included upstream)

 -- Emmanuel Lacour <elacour@home-dn.net>  Wed,  8 Nov 2006 10:06:32 +0100

suphp (0.6.1.20060928-1) unstable; urgency=low

  * New upstream snapshot (closes: #382086)
  * debian/control: updated Build-Depends (closes: #391019)
  * debian/control: updated suphp-common dependencies (closes: #375900)
  * debian/control: changes apache2-common depends in favor of
    apache2.2-common (closes: #391765)
  * debian/rules, debian/patches: Switching to dpatch
  * Removed debconf abuse (closes: #388967, closes: #383838)
  * debian/patches/04_apache2.2.dpatch: added support for apache 2.2.x
  * debian/libapache-mod-suphp.install: added suphp.conf in
    /etc/apache/conf.d/ (same behavior as apache2)

 -- Emmanuel Lacour <elacour@home-dn.net>  Sun,  8 Oct 2006 20:53:17 +0200

suphp (0.6.1-1) unstable; urgency=low

  * New upstream release 
  * libtool update (closes: #342676)

 -- Emmanuel Lacour <elacour@home-dn.net>  Thu, 29 Dec 2005 17:53:48 +0100

suphp (0.6.0-2) unstable; urgency=low

  * Added debian/po/fr.po (closes: #325213)
  * Added debian/po/cs.po (closes: #325870, closes: #331221)
  * Added debian/po/sv.po (closes: #330944, closes: #331264)
  * debian/suphp-common.config: lower down priority from "critical" to
    "medium"
  * debian/suphp-common.templates: better description for upgrade0.6.0
    (closes: #324635)
  * Added debian/NEWS with informations on 0.6.0 configuration changes
  * debian/control: use ${misc:Depends} instead of debconf (closes: #332107)

 -- Emmanuel Lacour <elacour@home-dn.net>  Sun,  2 Oct 2005 11:09:21 +0200

suphp (0.6.0-1) unstable; urgency=low

  * New upstream release (closes: #314471)
  * This new release introduce a runtime configuration file (closes: #297821,
    closes: #287888).
  * debian/rules: configure with php as /usr/bin/php-cgi (can also be set in
    the new runtime configuration file) (closes: #297790)
  * added a debconf note when upgrading from version << 0.6.0 which now
    require suPHP_AddHandler directive in apache configuration
  * src/apache2/mod_suphp.c, src/apache/mod_suphp.c: made suPHP_AddHandler
    able to be set as a global apache option
  * debian/control: depends on "php4 or php5" as php5 is now in unstable
  * debian/control: switch section from "admin" to "web"

 -- Emmanuel Lacour <elacour@home-dn.net>  Sat, 13 Aug 2005 13:42:30 +0200

suphp (0.5.2-3) unstable; urgency=low

  * rules: rename INSTALL files to README
  * control: suppress leading spaces at beginning of description
  * add lintian-override for suid binary
  * remove debconf for issuing broken apache2 config warning
  * rules: remove Makefile and config.(status|log|h) in clean target
  * suphp-common.copyright: make difference between "license" and "copyright"
  * suphp-common.postrm: cleanup logs on purge
  

 -- Emmanuel Lacour <elacour@home-dn.net>  Tue, 12 Oct 2004 10:40:47 +0200

suphp (0.5.2-2) unstable; urgency=low

  * Move /usr/sbin/suphp to /usr/lib/suphp/suphp as this binary isn't for
    command line use
  * Added man page for suphp command
  * Add postinst/prerm script do libapache*-mod-suphp

 -- Emmanuel Lacour <elacour@home-dn.net>  Mon, 20 Sep 2004 21:59:24 +0200

suphp (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Emmanuel Lacour <elacour@home-dn.net>  Wed, 14 Jul 2004 01:25:55 +0200

suphp (0.5.1-1) unstable; urgency=low

  * New upstream release

 -- Emmanuel Lacour <elacour@home-dn.net>  Sun, 29 Feb 2004 20:28:17 +0100

suphp (0.3-1) unstable; urgency=low

  * Initial Release.

 -- Emmanuel Lacour <elacour@home-dn.net>  Fri, 19 Sep 2003 15:25:44 +0200

