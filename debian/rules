#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)


ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

patch:
	dpatch apply-all

configure-apache2: patch
	dh_testdir
	CPPFLAGS="`pkg-config --cflags apr-1`" ./configure --prefix=/usr \
	--sysconfdir=/etc/suphp \
	--with-apxs=/usr/bin/apxs2 \
	--with-apache-user=www-data \
	--with-php=/usr/bin/php-cgi \
	--sbindir=/usr/lib/suphp \
	--with-setid-mode=force \
	--with-logfile=/var/log/suphp/suphp.log


build-apache2: configure-apache2
	dh_testdir
	$(MAKE)

build:

clean:
	dh_testdir
	dh_testroot
	[ ! -f Makefile ] || $(MAKE) clean
	rm -f config.log config.status src/config.h
	rm -f libtool src/stamp-h
	find . -name .libs | xargs rm -rf
	find . -name .deps | xargs rm -rf
	rm -f src/apache/mod_suphp.o src/apache/mod_suphp.lo src/apache/mod_suphp.la src/stamp-h1
	find . -name Makefile | xargs rm -f
	dh_clean
	dpatch deapply-all
	rm -rf debian/patched

install: build-apache2
	dh_testdir
	dh_testroot
	dh_prep
#	find -name ".svn" -type d | xargs rm -rf
#	find -name ".git" -type d | xargs rm -rf
	dh_installdirs
	dh_install
	docbook-to-man debian/suphp-common.manpage.sgml > debian/suphp-common/usr/share/man/man8/suphp.8
	install -m 644 debian/suphp-common.lintian-override debian/suphp-common/usr/share/lintian/overrides/suphp-common
	mv debian/suphp-common/etc/suphp/suphp.conf-example debian/suphp-common/etc/suphp/suphp.conf
	mv debian/libapache2-mod-suphp/usr/lib/apache2/modules/mod_suphp.so.0.0.0 debian/libapache2-mod-suphp/usr/lib/apache2/modules/mod_suphp.so


# Build architecture-independent files here.
binary-indep: 
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: install
	dh_testdir
	dh_testroot
#	dh_installdebconf	
	dh_installdocs
	mv debian/libapache2-mod-suphp/usr/share/doc/libapache2-mod-suphp/INSTALL debian/libapache2-mod-suphp/usr/share/doc/libapache2-mod-suphp/README
#	dh_installexamples
#	dh_installmenu
	dh_installlogrotate
#	dh_installemacsen
#	dh_installpam
#	dh_installmime
#	dh_installinit
#	dh_installcron
#	dh_installman
#	dh_installinfo
	dh_installchangelogs ChangeLog
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	chmod u+s debian/suphp-common/usr/lib/suphp/suphp
#	dh_makeshlibs
	dh_installdeb
#	dh_perl
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: configure build build-apache2 clean binary-indep binary-arch binary install patch unpatch
